const AWS = require('aws-sdk');
const s3 = new AWS.S3();


exports.handler = async (event) => {

    try {

        console.log("Processing webhook. event:", event);
        const { as2From, as2To, messageId, subject, partnerType, fileName } = parsingHeaders(event['headers']);
        const { body, isBase64Encoded } = event;

        console.log("Parsed data:", JSON.stringify({ as2From, as2To, messageId, subject, partnerType, fileName }));

        let data = await s3.putObject({
            Bucket: "as2-webhook-payload-processor",
            Key: generateFilePath({ as2From, as2To, messageId, fileName }),
            Body: isBase64Encoded ? Buffer.from(body, 'base64') : body,
            Metadata: { as2From, as2To, messageId, subject, partnerType, fileName }
        }).promise();

        console.log("File is saved successfully")
        return getSuccessResponse(data);

    } catch (err) {
        console.log("Something went wrong", err)
        return getFailureResponse({ "Error": err })
    };
};


const parsingHeaders = (headers) => {
    if (headers) {
        return {
            as2From: headers['as2-from'] || headers['AS2-From'] || 'Unknown',
            as2To: headers['as2-to'] || headers['AS2-To'] || 'Unknown',
            messageId: headers['message-id'] || headers['Message-ID'] || Date.now(),
            subject: headers['subject'] || headers['Subject'] || 'No subject',
            partnerType: headers['x-partner-type'] || headers['X-Partner-Type'] || 'Unknown',
            fileName: getFileName(headers['content-disposition'] || headers['Content-Disposition'])
        }
    } else {
        throw new Error('Required headers not found');
    }
}

const getFileName = (contentHeader) => {
    if(!contentHeader) {
        return 'Unknown';
    }
    return contentHeader.substring(contentHeader.lastIndexOf('=') + 1);
}

const getSuccessResponse = (data) => {
    return {
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*"
        },
        "statusCode": 200,
        "body": JSON.stringify(data)
    };
}

const getFailureResponse = (data) => {
    return {
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*"
        },
        "statusCode": 500,
        "body": JSON.stringify(data)
    }
}

const generateFilePath = ({ as2From, as2To, messageId, fileName }) => {
    return `${as2To}/${as2From}/${messageId}/${fileName}`
}

